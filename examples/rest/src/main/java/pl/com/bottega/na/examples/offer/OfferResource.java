package pl.com.bottega.na.examples.offer;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Getter;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

class OfferResource extends ResourceSupport {

    @Getter
    @JsonUnwrapped
    private Offer content;

    OfferResource(Offer content) {
        this.content = content;
        add(linkTo(methodOn(OfferEndpoint.class).getOffer(content.getId())).withSelfRel());
        if (!content.isPublished()) {
            add(linkTo(methodOn(OfferEndpoint.class).publish(content.getId())).withRel("publish"));
        }
    }
}
