= Sample API Documentation
:toc: left
:sectnums:

== Introduction
This is an example of Spring REST Docs generated documentation.

=== Add offer

==== Sample Request
include::{snippets}/OfferEndpointTest/shouldAddOffer/http-request.adoc[]

==== Sample Response
include::{snippets}/OfferEndpointTest/shouldAddOffer/http-response.adoc[]

===== Response Fields
include::{snippets}/OfferEndpointTest/shouldAddOffer/response-fields.adoc[]

==== CURL sample
include::{snippets}/OfferEndpointTest/shouldAddOffer/curl-request.adoc[]