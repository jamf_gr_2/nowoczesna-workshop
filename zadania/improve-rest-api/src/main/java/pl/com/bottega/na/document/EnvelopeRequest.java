package pl.com.bottega.na.document;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

@Data
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "action")
@JsonSubTypes({
        @JsonSubTypes.Type(name = CreateDocumentRequest.ACTION, value = CreateDocumentRequest.class),
        @JsonSubTypes.Type(name = FindDocumentsRequest.ACTION, value = FindDocumentsRequest.class),
        @JsonSubTypes.Type(name = VerifyDocumentRequest.ACTION, value = VerifyDocumentRequest.class)
})
abstract class EnvelopeRequest {

}
